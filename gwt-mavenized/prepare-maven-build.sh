#!/bin/bash
#
# Petr Kozelka (C) 2007
# pkozelka@gmail.com
#
# Performs the original ANT build, in order to create and install artifacts that are not mavenized yet.
# Note that currently only sample apps are mavenized.
#
export GWT_VERSION="1.5-SNAPSHOT"

mydir=${0%/*}
cd $mydir
mydir="$(pwd)"
export GWT_TOOLS="$mydir/gwt-external-tools"

cd gwt-external
ant $@ clean build || exit -1
libs="$(pwd)/build/lib"
mvn install:install-file -Dpackaging=jar -DgroupId=com.google.gwt -Dversion=$GWT_VERSION -DartifactId=gwt-user -Dfile=$libs/gwt-user.jar || exit -1
mvn install:install-file -Dpackaging=jar -DgroupId=com.google.gwt -Dversion=$GWT_VERSION -DartifactId=gwt-servlet -Dfile=$libs/gwt-servlet.jar || exit -1
for i in linux windows mac; do
	mvn install:install-file -Dpackaging=jar -DgroupId=com.google.gwt -Dversion=$GWT_VERSION -DartifactId=gwt-dev-$i -Dfile=$libs/gwt-dev-$i.jar || exit -1
done
