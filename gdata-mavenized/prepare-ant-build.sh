#!/bin/bash
#
# Petr Kozelka (C) 2007
# pkozelka@gmail.com
#
# Creates directory with links to non-distributable dependencies for gdata, assuming 
# you already have them available in your local Maven 2 repository.
# 2007 (C) Petr Kozelka <pkozelka@gmail.com>
# http://code.google.com/p/google-apis-mavenized/
#

DEP="/tmp/gdata_dep"
M2_HOME="$HOME/.m2"
rm -rf "$DEP"
mkdir -p "$DEP"
ln -s "$M2_HOME/repository/javax/activation/activation/1.1/activation-1.1.jar" "$DEP/activation.jar"
ln -s "$M2_HOME/repository/javax/mail/mail/1.4/mail-1.4.jar" "$DEP/mail.jar"
