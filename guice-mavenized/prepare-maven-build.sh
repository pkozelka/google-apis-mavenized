#!/bin/bash
#
# Petr Kozelka (C) 2007
# pkozelka@gmail.com
#
# This script performs all necessary workarrounds to ensure successful execution of "mvn clean install"
#

M2_HOME="$HOME/.m2"

# Install cglib-2.2 as it has not been published in central repository yet
if ! [ -s "$M2_HOME/repository/cglib/cglib/2.2/cglib-2.2.jar" ]; then
	echo "Installing cglib-2.2"
	mvn install:install-file -DgroupId=cglib -DartifactId=cglib -Dversion=2.2 -Dpackaging=jar -Dfile=guice-external/lib/build/cglib-2.2.jar
fi