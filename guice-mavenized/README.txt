Mavenization notes on GUICE
- versions set to snapshot according to issue google-guice#92
- parent pom defines consistent library versions in its dependencyManagement section
- this work is based on contributions posted to discussion of issue google-guice#59
- jarjar stuff ignored
- before running maven, run the prepare-build.sh script
